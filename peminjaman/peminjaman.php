<?php
include ('cek.php');
error_reporting(0);
session_start();
?>
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Inventaris</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

    <script src="lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $(".knob").knob();
        });
    </script>


    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
</head>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
    #line-chart {
        height:300px;
        width:800px;
        margin: 0px auto;
        margin-top: 1em;
    }
    .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
        color: #fff;
    }
</style>

<script type="text/javascript">
    $(function() {
        var uls = $('.sidebar-nav > ul > *').clone();
        uls.addClass('visible-xs');
        $('#main-menu').append(uls.clone());
    });
</script>

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <!-- Le fav and touch icons -->
  <link rel="shortcut icon" href="../assets/ico/favicon.ico">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
      <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
          <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
              <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
                  <!--[if (gt IE 9)|!(IE)]><!--> 

                  <!--<![endif]-->

                  <div class="navbar navbar-default" role="navigation" style="background: #000000 ;"> 
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="" href="index.php"><span class="navbar-brand"><span class="fa fa-briefcase"></span> Inventaris Sarana dan Prasarana</span></a></div>

                    <div class="navbar-collapse collapse" style="height: 1px;">
                      <ul id="main-menu" class="nav navbar-nav navbar-right">
                        <li class="dropdown hidden-xs">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i> <?php echo $_SESSION['username'];?> 
                                <i class="fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">


                                <li>
                                <a href="#myModal" role="button" data-toggle="modal">
                                      <i class="ace-icon fa fa-power-off"></i>
                                      Logout
                                  </a>
                              </li>
                          </ul>
                      </li>
                  </ul>

              </div>
          </div>
      </div>


    <div class="header">

<br>
		 <?php

       if ($_SESSION['id_level']==3): ?>

        <?php echo ' <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Selamat Datang </h4>
                        	Di Peminjaman Sarana dan Prasarana SMK</div>';?>
		<?php endif; ?>
        <ul class="breadcrumb">

        </ul>

    </div>
    <div class="main-content">

    </div>  <div class="panel-body">
       <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Form Peminjaman                       
                </div>
                <div class="panel-body">
                    <center><div class="panel-body">
                        <div class="col-lg-3 col-md-offset-4">
                            <label>Pilih Nama Barang</label>
                            <form method="POST">
                                <select name="id_inventaris" class="form-control m-bot15">
                                    <?php
                                    include "koneksi.php";
                                //display values in combobox/dropdown
                                    $result = mysql_query("SELECT id_inventaris,nama from inventaris where jumlah > 0");
                                    while($row = mysql_fetch_assoc($result))
                                    {
                                        echo "<option value='$row[id_inventaris]'>$row[nama]</option>";
                                    } 
                                    ?>
                                </select>
                                <br/>
                                <button type="submit" name="pilih" class="btn btn-outline btn-success">Pilih</button>
                            </form>
                        </div>
                    </div></center>

                    <?php
                    if(isset($_POST['pilih'])){?>
                      <form action="simpan_smtr_detail.php" method="post" enctype="multipart/form-data">
                       <?php
                       include "koneksi.php";
                       $id_inventaris=$_POST['id_inventaris'];
                       $select=mysql_query("select * from inventaris where id_inventaris='$id_inventaris'");
                       while($data=mysql_fetch_array($select)){
                        ?>
                        <br>
                        <br>
                          <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        ID Inventaris
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input name="id_inventaris" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
                         <input name="id_detail_pinjam" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly"></div>
                </div><br><br>
                        
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        Nama Barang
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input name="" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
    
                    </div>
                </div><br><br>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        Jumlah Barang
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input name="jumlah" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['jumlah'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
                    </div>
                </div><br><br>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        Jumlah Pinjam
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input name="jumlah_pinjam" required="harus diisi" type="number" class="form-control" placeholder="Jumlah" autocomplete="off" maxlength="11">
                    
                    </div>
                </div><br>
            
                      
                              <br>&nbsp;&nbsp;<button type="submit" class="btn">Tambah</button>
                              <br>
                              <br>

                          <?php  } ?>
                      </form>

                  <?php } ?>
              </div>
              <form action="simpan_pinjam.php" method="post" role="form">
                <?php 
                include "koneksi.php";
                $status_peminjaman='Pinjam';
                $status="Y";
                $query = "SELECT max(kode_pinjam) as maxKode FROM peminjaman";
                $hasil = mysql_query($query);
                $data = mysql_fetch_array($hasil);
                $kode_barang = $data['maxKode'];
                $noUrut = (int) substr($kode_barang, 4, 4);
                $noUrut++;
                $char = "PNJ";
                $kode_barang = $char . sprintf("%04s", $noUrut);
                ?>


                <br><br>
                <div class="row">
                  <div class="panel-body">
                      <div class="table-responsive">

                        <div class="col-lg-12">

                          <?php 
                          
                          $kode_pinjam=$_POST['kode_pinjam'];
                          
                          $auto=mysql_query("select * from peminjaman order by id_peminjaman desc limit 1");
                          $no=mysql_fetch_array($auto);
                          $angka=$no['id_peminjaman']+1;  ?>
                          <div class="row" style="margin-left: 550px">
                            
                            <div class="col-md-6">Kode Pinjam<input name="kode_pinjam" readonly type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $kode_barang;?>" autocomplete="off" maxlength="11">
                            </div>
                            <div class="col-md-5">Nama Pegawai<select name="id_pegawai" class="form-control m-bot15">
                            <option>---Pilih---</option>
                            <?php
                            include "koneksi.php";
                                //display values in combobox/dropdown
                            $result = mysql_query("SELECT id_pegawai,nama_pegawai from pegawai ");
                            while($row = mysql_fetch_assoc($result))
                            {
                                echo "<option>$row[id_pegawai].$row[nama_pegawai]</option>";
                            } 
                            ?>

                        </select></div></div>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID Detail Pinjam</th>
                                        <th>Nama Barang</th>
                                        <th>Jumlah</th>
                                        <th>Jumlah Pinjam</th>
                                        <th>Option</th>

                                    </tr> 
                                </thead>
                                <tbody>
                                    <?php
                                    include "koneksi.php";
                                    $select=mysql_query("select * from smtr_detail left join  
                                        inventaris on smtr_detail.id_inventaris=inventaris.id_inventaris");
                                    while($data=mysql_fetch_array($select))
                                    {
                                        ?>
                                        <tr>
                                           <td><input name="id_detail_pinjam[]" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $angka;?>" autocomplete="off" maxlength="11" required="" readonly>
                                            <input name="status[]" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $status;?>" autocomplete="off" maxlength="11" required="" readonly>
                                               <input name="tanggal_kembali" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" autocomplete="off" maxlength="11" readonly="readonly">
                                                <input name="tanggal_pinjam" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" autocomplete="off" maxlength="11" readonly="readonly">
                                               <input name="id_peminjaman" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $angka;?>" autocomplete="off" maxlength="11">
                                               <input name="status_peminjaman" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $status_peminjaman;?>" autocomplete="off" maxlength="11">
                                               <input name="id_inventaris[]" id="id_inventaris" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>" autocomplete="off" maxlength="11"></td>
                                               <td><input name=""  type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" readonly></td>
                                                   <td><input name="jumlah[]" id="jumlah" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['jumlah'];?>" autocomplete="off" maxlength="11" readonly ></td>
                                                       <td><input name="jumlah_pinjam[]" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['jumlah_pinjam'];?>" autocomplete="off" maxlength="11" ></td>
                                                       
                                                      <td>
                                                       <a class="btn btn outline btn-info  glyphicon glyphicon-remove" href="hapus_smtr_detail.php?id_detail_pinjam=<?php echo $data['id_detail_pinjam']; ?>"></a>
                                                     </td>

                                                       </tr>
                                                       
                                                        <?php
                                                   }
                                                   ?>
                                               
                                           </tbody>

                                       </table>
                                       </form>
                     <?php
                     $query=mysql_query("SELECT * FROM smtr_detail");
                     $data=mysql_fetch_array($query);
                     $cek = mysql_num_rows($query);
                    if($cek >0){
                     ?>
                                       <button type="submit" class="btn btn-warning">&nbsp;Pinjam</button>
                    <?php }?>
                                       <hr><br>
                                       <br>
                                  
                                  

                                   <!-- /.panel-heading -->
                                   <div class="panel-body">
                                    <div class="table-responsive">

                                      
                                            <table class="table">
                                              <thead>
                                                <tr>
                                                  <th>No.</th>
                                                  <th>Kode Pinjam</th>
                                                  <th>Tanggal Pinjam</th>
                                                  <th>Nama Pegawai</th>
                                                  <th>Status Peminjaman</th>
                                                  <th style="width: 3.5em;"></th>
                                                  <br>
                                                  
                                                  
                                                  <hr>
                                              </tr> 
                                          </thead>
                                          <tbody>
                                           <?php
                                           include "koneksi.php";
                                           $select=mysql_query("select * from peminjaman s left join pegawai p on p.id_pegawai=s.id_pegawai
                                             where status_peminjaman='Pinjam'");
                                           $no=1;
                                           while($data=mysql_fetch_array($select))
                                           {
                                            ?>
                                            <tr>
                                             <td><?php echo $no++; ?></td>
                                             <td><?php echo $data['kode_pinjam'];?></td>
                                             <td><?php echo $data['tanggal_pinjam']; ?></td>
                                             <td><?php echo $data['nama_pegawai']; ?></td>
                                             <td><span class="label label-success"><?php echo $data['status_peminjaman']; ?></td>
                                                

                                             </tr>
                                             <?php
                                         }
                                         ?>



                                     </tbody>
                                 </table>
                             </div>
                         </div>
                     </div>
                 </div>
                  <div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Logout Confirmation</h3>
        </div>
 <div class="modal-body">
            <p class="error-text"><i class="fa fa-warning modal-icon"></i><?php echo $_SESSION['username'];?> yakin ingin keluar dari webiste ini?
        </div>
        Silahkan Klik Button Logout
        <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Kembali</button>
            <a class="btn btn-warning" href="logout.php">Logout</a>
           
        </div>
      </div>
    </div>
</div>
                 <script type="text/javascript" src="assets/js/jquery.js"></script>
                 <script type="text/javascript" src="assets/js/jquery.min.js"></script>
                 <script type="text/javascript" src="assets/js/jquery.dataTables.js"></script>
                 <script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>

                 <script>
                    $(document).ready(function() {
                        $('#example').DataTable();
                    });
                </script>



                <footer>
                    <hr>

                    <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                    <p class="pull-right">Inventaris</a> 
                        <p> © 2018 Sarana & Prasarana</a></p>
                    </footer>
                </div>
            </div>


            <script src="lib/bootstrap/js/bootstrap.js"></script>
            <script type="text/javascript">
                $("[rel=tooltip]").tooltip();
                $(function() {
                    $('.demo-cancel-click').click(function(){return false;});
                });
            </script>

        </body>

        </html>