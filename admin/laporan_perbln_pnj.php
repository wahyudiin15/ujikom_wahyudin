<?php
include ('cek.php');
error_reporting(0);
session_start();
?>

<?php
include ('cek_level.php');
?>
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Inventaris</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

    <script src="lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $(".knob").knob();
        });
    </script>


    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
</head>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
    #line-chart {
        height:300px;
        width:800px;
        margin: 0px auto;
        margin-top: 1em;
    }
    .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
        color: #fff;
    }
</style>

<script type="text/javascript">
    $(function() {
        var uls = $('.sidebar-nav > ul > *').clone();
        uls.addClass('visible-xs');
        $('#main-menu').append(uls.clone());
    });
</script>

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <!-- Le fav and touch icons -->
  <link rel="shortcut icon" href="../assets/ico/favicon.ico">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
      <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
          <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
              <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
                  <!--[if (gt IE 9)|!(IE)]><!--> 

                  <!--<![endif]-->

                  <div class="navbar navbar-default" role="navigation" style="background: #008080 ;">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="" href="index.php"><span class="navbar-brand"><span class="fa fa-briefcase"></span> Inventaris Sarana dan Prasarana</span></a></div>

                    <div class="navbar-collapse collapse" style="height: 1px;">
                      <ul id="main-menu" class="nav navbar-nav navbar-right">
                        <li class="dropdown hidden-xs">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i> <?php echo $_SESSION['nama_petugas'];?> 
                                <i class="fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">


                                <li>
                                    <a href="#myModal" role="button" data-toggle="modal">
                                        <i class="ace-icon fa fa-power-off"></i>
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                </div>
            </div>
        </div>

        <div class="sidebar-nav">
           <?php

           if ($_SESSION['id_level']==1){

            echo'
            <ul>
            <li><a href="index.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i>&nbsp Dashboard</a></li>
            <li>
            <li><a href="#" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-briefcase"></i> Inventarisir<i class="fa fa-collapse"></i></a></li>
            <li><ul class="dashboard-menu nav nav-list collapse">
            <li ><a href="inventaris.php"><span class="fa fa-caret-right"></span> Data Barang</a></li>
            <li><a href="jenis.php"><span class="fa fa-caret-right"></span> Data Jenis</a></li>
            <li ><a href="ruang.php"><span class="fa fa-caret-right"></span> Data Ruang</a></li>
            </ul></li>
            <li><a href="peminjaman.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-plus"></i>&nbsp Peminjaman</a></li>
            <li>
            <li><a href="pengembalian.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-reply"></i>&nbsp Pengembalian</a></li>
            <li>
            <li><a href="#" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-paste"></i> Laporan<i class="fa fa-collapse"></i></a></li>
            <li><ul class="dashboard-menu nav nav-list collapse">
            <li ><a href="laporan_barang.php"><span class="fa fa-caret-right"></span> Laporan Barang</a></li>
            <li><a href="laporan_peminjam.php"><span class="fa fa-caret-right"></span> Laporan Peminjaman</a></li>
            </ul></li>

            <li>
            <li><a href="#" data-target=".accounts-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-list"></i> Lainnya<i class="fa fa-collapse"></i></a></li>
            <li><ul class="accounts-menu nav nav-list collapse">
            <li ><a href="petugas.php"><span class="fa fa-caret-right"></span> Petugas</a></li>
            <li><a href="pegawai.php"><span class="fa fa-caret-right"></span> Pegawai</a></li>
            <li><a href="backup.php"><span class="fa fa-caret-right"></span> Backup</a></li>
            </ul></li>
            </div>

            </ul></li>


            </ul></li>';
        }

        elseif ($_SESSION['id_level']==2){
           echo'
           <ul>
           <li><a href="index.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i>&nbsp Dashboard</a></li>
           <li>
           <li><a href="peminjaman.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-plus"></i>&nbsp Peminjaman</a></li>
           <li>
           <li><a href="pengembalian.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-plus"></i>&nbsp Pengembalian</a></li>
           <li></ul>';
       }
       elseif ($_SESSION['id_level']==3){
           echo'<ul>
           <li><a href="index.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i>&nbsp Dashboard</a></li>
           <li>
           <li><a href="peminjaman.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-plus"></i>&nbsp Peminjaman</a></li>
           <li></ul>';
       }
       ?>



   </div>

   <div class="content">
    <div class="header">


        <h1 class="page-title">Laporan Data Peminjam</h1>
        <ul class="breadcrumb">

        </ul>

    </div>



    <div class="col-lg-12">


        <br>
    </div>

    <div class="panel-body">
        <div class="table-responsive">
            <div class="btn-group">
                <button type="button" class="btn btn-default btn-sm btn-flat dropdown-toggle" data-toggle="dropdown">
                    Laporan Peminjam <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#"></a></li>
                    <li><a href="laporan_pertgl_pnj.php">Laporan Per Tanggal</a></li>
                    <li class="divider"></li>
                    <li><a href="laporan_perbln_pnj.php">Laporan Per Bulan</a></li>
                    <li class="divider"></li>
                    <li><a href="laporan_perthn_pnj.php">Laporan Per Tahun</a></li>
                </ul>
            </div>



            <br><br>
            <center><div class="panel-body">
                <div class="col-lg-3 col-md-offset-4">
                    <label>Pilih Bulan</label>
                    <form method="get">
                        <select name="bulan" class="form-control m-bot15">
                            <?php
                            include "koneksi.php";
                                //display values in combobox/dropdown
                            $result = mysql_query("SELECT MONTH (tanggal_pinjam) as bulan from peminjaman GROUP BY MONTH(tanggal_pinjam)");
                            while($row = mysql_fetch_assoc($result))
                            {
                                echo "<option value='$row[bulan]'>$row[bulan]</option>";
                            } 
                            ?>
                        </select>
                        <br/>
                        <button type="submit" class="btn btn-outline btn-success">Tampilkan</button>
                    </form>
                </div>
            </div></center>


            
            <table id="example" class="table table-striped  table-colored table-info">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode Pinjam</th>
                        <th>Nama Barang</th>
                        <th>Jumlah Pinjam</th>
                        <th>Tanggal Pinjam</th>
                        <th>Tanggal Kembali</th>
                        <th>Nama Pegawai</th>
                  



                    </tr>
                </thead>


                <tbody>
                  <?php
                  $no=1;
                  if(isset($_GET['bulan'])){                                
                    $select = mysql_query("select * from peminjaman left join detail_pinjam on detail_pinjam.id_detail_pinjam=peminjaman.id_peminjaman 
                    left join inventaris on detail_pinjam.id_inventaris=inventaris.id_inventaris 
                    left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai
                                              where MONTH(tanggal_pinjam) ='$_GET[bulan]'");
                }else{
                    $select=mysql_query("select * from peminjaman left join detail_pinjam on detail_pinjam.id_detail_pinjam=peminjaman.id_peminjaman 
                    left join inventaris on detail_pinjam.id_inventaris=inventaris.id_inventaris 
                    left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai
                                             ");
                }

                while($lihat = mysql_fetch_array($select)){
                    ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $lihat['kode_pinjam']; ?></td>
                        <td><?php echo $lihat['nama']; ?></td>
                        <td><?php echo $lihat['jumlah_pinjam']; ?></td>
                        <td><?php echo $lihat['tanggal_pinjam']; ?></td>
                        <td><?php echo $lihat['tanggal_kembali']; ?></td>
                        <td><?php echo $lihat['nama_pegawai']; ?></td>
                        

                    </tr>


                    <?php
                }
                ?>





            </tbody>

        </table>






    </div>

    <form method="POST" action="export_blnpnj.php" style="
    display: inline-block;
">
        <input type="hidden" name="bulan" value=<?php echo $_GET['bulan']?>>
        <button type="submit" class="btn btn-warning fa fa-paste">&nbsp;&nbsp;Export To Excel</button>
    </form>

    <form method="POST" action="cetak_blnpnj.php" style="
    display: inline-block;
">
        <input type="hidden" name="bulan" value=<?php echo $_GET['bulan']?>>
        <button type="submit" class="btn btn-danger fa fa-print">&nbsp;&nbsp;Cetak PDF</button>
    </form>







</div> <!-- container -->

</div> <!-- content -->




</div>



<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Logout Confirmation</h3>
        </div>
        <div class="modal-body">
            <p class="error-text"><i class="fa fa-warning modal-icon"></i><?php echo $_SESSION['nama_petugas'];?> yakin ingin keluar dari webiste ini?
            </div>
            Silahkan Klik Button Logout
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Kembali</button>
              <a class="btn btn-warning" href="logout.php">Logout</a>

          </div>
      </div>
  </div>
</div>


<footer>
    <hr>

    <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
    <p class="pull-right">Inventaris</a> 
        <p> © 2019 Sarana & Prasarana</a></p>
    </footer>
</div>
</div>


<script src="lib/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
    $("[rel=tooltip]").tooltip();
    $(function() {
        $('.demo-cancel-click').click(function(){return false;});
    });
</script>

</body>

</html>
