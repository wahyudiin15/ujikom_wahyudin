<?php
include 'koneksi.php';
require('pdf/fpdf.php');

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('pdf/smkn1ciomas.jpg',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'INVENTARIS SMK',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'UJIKOM 2019',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'SMKN 1 CIOMAS',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Jalan Raya Laladon No. 2 RT 04/06 Desa Laladon Kec. Ciomas Kab. Bogor',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data Peminjam",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Kode Pinjam', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama Barang', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Jumlah Pinjam', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Tanggal Pinjam', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Tanggal Kembali', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama pegawai', 1, 0, 'C');
$pdf->SetFont('Arial','',10);
 $no=1;
 $select=mysql_query("select * from peminjaman left join detail_pinjam on detail_pinjam.id_detail_pinjam=peminjaman.id_peminjaman 
                    left join inventaris on detail_pinjam.id_inventaris=inventaris.id_inventaris 
                    left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai
                                             ");

while($lihat=mysql_fetch_array($select)){
	$pdf->ln(0.8);
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['kode_pinjam'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['nama'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['jumlah_pinjam'],1, 0, 'C');
	$pdf->Cell(4, 0.8, $lihat['tanggal_pinjam'], 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['tanggal_kembali'], 1, 0,'C');
	$pdf->Cell(3, 0.8, $lihat['nama_pegawai'],1, 0, 'C');






	$no++;
}

$pdf->Output("cetak_peminjam.pdf","D");

?>

