DROP TABLE deskripsi;

CREATE TABLE `deskripsi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO deskripsi VALUES("2","<p><span style=\"font-size:16px\">Inventarisasi berasal dari kata &quot;inventaris&quot; (Latin = inventarium) yang berarti daftar barang-barang,bahan dan sebagainya. Inventarisasi sarana dan prasarana pendidikan adalah pencatatan atau pendaftaran barang-barang milik sekolah ke dalam suatu daftar inventaris barang secara tertib dan teratur menurut ketentuan dan tata cara yang berlaku. Barang inventaris sekolah adalah semua barang milik negara (yang dikuasai sekolah) baik yang diadakan/dibeli melalui dana dari pemerintah,DPP maupun diperoleh sebagai pertukaran,hadiah atau hibah serta hasil usaha pembuatan sendiri di sekolah guna menunjang kelancaran proses belajar mengajar.</span></p>\n");
INSERT INTO deskripsi VALUES("3","<p><span style=\"font-size:16px\">Level - level Pengguna</span></p>\n\n<ol>\n	<li><span style=\"font-size:16px\">Administrator</span></li>\n	<li><span style=\"font-size:16px\">Operator</span></li>\n	<li><span style=\"font-size:16px\">Peminjam</span></li>\n</ol>\n");



DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` int(11) NOT NULL,
  `status` varchar(11) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`),
  KEY `id_inventaris` (`id_inventaris`),
  CONSTRAINT `detail_pinjam_ibfk_1` FOREIGN KEY (`id_inventaris`) REFERENCES `inventaris` (`id_inventaris`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("1","1","22","2","Y");
INSERT INTO detail_pinjam VALUES("2","2","22","1","Y");
INSERT INTO detail_pinjam VALUES("3","2","23","1","Y");
INSERT INTO detail_pinjam VALUES("4","2","26","1","Y");
INSERT INTO detail_pinjam VALUES("5","2","27","1","Y");
INSERT INTO detail_pinjam VALUES("6","3","22","1","Y");
INSERT INTO detail_pinjam VALUES("7","4","26","1","Y");
INSERT INTO detail_pinjam VALUES("8","5","22","1","Y");
INSERT INTO detail_pinjam VALUES("9","5","23","1","Y");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kondisi` varchar(20) NOT NULL,
  `keterangan_inventaris` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_petugas` (`id_petugas`),
  CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`) ON UPDATE CASCADE,
  CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`) ON UPDATE CASCADE,
  CONSTRAINT `inventaris_ibfk_3` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("22","Laptop","Baik","Barang Masuk","247","1","2019-03-07","1","INV0001","1");
INSERT INTO inventaris VALUES("23","Papan Tulis","Baik","Barang Masuk","300","5","2019-03-07","3","INV0002","1");
INSERT INTO inventaris VALUES("24","Proyektor","Baik","Barang Masuk","250","1","2019-03-07","2","INV0003","1");
INSERT INTO inventaris VALUES("26","Headshet","Baik","Barang Masuk","298","1","2019-03-08","1","INV0004","1");
INSERT INTO inventaris VALUES("27","Meja","Baik","Barang Masuk","299","5","2019-03-30","3","INV0005","1");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(35) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(45) NOT NULL,
  `kode_jenis` varchar(45) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("1","Elektronik","K-001","Beli Barang");
INSERT INTO jenis VALUES("5","Kayu","K-004","Beli Barang");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(35) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","administrator");
INSERT INTO level VALUES("2","operator");
INSERT INTO level VALUES("3","peminjam");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` int(15) NOT NULL,
  `alamat` text NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("1","Lastri Fauziah","1150701","Ciomas Permai");
INSERT INTO pegawai VALUES("2","Riskyawati","1191001","Gunung Batu");
INSERT INTO pegawai VALUES("3","Reyhan Hakiim","11020601","Bukit Asri");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `kode_pinjam` varchar(15) NOT NULL,
  `tanggal_pinjam` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `tanggal_kembali` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status_peminjaman` varchar(15) NOT NULL DEFAULT 'Pinjam',
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_pegawai` (`id_pegawai`),
  CONSTRAINT `peminjaman_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("1","PNJ0001","2019-04-01 11:33:14","2019-04-01 11:33:14","Kembali","1");
INSERT INTO peminjaman VALUES("2","PNJ0002","2019-04-01 04:27:09","0000-00-00 00:00:00","Pinjam","1");
INSERT INTO peminjaman VALUES("3","PNJ0003","2019-04-01 05:51:20","0000-00-00 00:00:00","Pinjam","1");
INSERT INTO peminjaman VALUES("4","PNJ0004","2019-04-01 06:20:12","0000-00-00 00:00:00","Pinjam","1");
INSERT INTO peminjaman VALUES("5","PNJ0005","2019-04-01 08:55:01","0000-00-00 00:00:00","Pinjam","1");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(35) NOT NULL,
  `password` varchar(35) NOT NULL,
  `nama_petugas` varchar(40) NOT NULL,
  `id_level` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_pegawai` (`id_level`),
  CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","wahyudin","wahyudin123","Admin","1","muhamadwahyudin015@gmail.com");
INSERT INTO petugas VALUES("2","resky","resky123","Operator","2","reskysaepudin.23@gmail.com");
INSERT INTO petugas VALUES("3","ilham","ilham123","Peminjam","3","muhammadariff722@gmail.com");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(20) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(15) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Lab","LB-001","Beli Barang");
INSERT INTO ruang VALUES("2","TU","TU-001","Masukan Barang");
INSERT INTO ruang VALUES("3","Kelas","KLS-001","Beli Barang");
INSERT INTO ruang VALUES("4","Kantor","KTR-003","Beli Barang");
INSERT INTO ruang VALUES("5","Perpustakaan","PRPS-005","Beli Barang");



DROP TABLE smtr_detail;

CREATE TABLE `smtr_detail` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




