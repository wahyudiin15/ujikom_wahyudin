DROP TABLE deskripsi;

CREATE TABLE `deskripsi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO deskripsi VALUES("2","<p><span style=\"font-size:16px\">Inventarisasi berasal dari kata &quot;inventaris&quot; (Latin = inventarium) yang berarti daftar barang-barang,bahan dan sebagainya. Inventarisasi sarana dan prasarana pendidikan adalah pencatatan atau pendaftaran barang-barang milik sekolah ke dalam suatu daftar inventaris barang secara tertib dan teratur menurut ketentuan dan tata cara yang berlaku. Barang inventaris sekolah adalah semua barang milik negara (yang dikuasai sekolah) baik yang diadakan/dibeli melalui dana dari pemerintah,DPP maupun diperoleh sebagai pertukaran,hadiah atau hibah serta hasil usaha pembuatan sendiri di sekolah guna menunjang kelancaran proses belajar mengajar.</span></p>\n");
INSERT INTO deskripsi VALUES("3","<p><span style=\"font-size:16px\">Level - level Pengguna</span></p>\n\n<ol>\n	<li><span style=\"font-size:16px\">Administrator</span></li>\n	<li><span style=\"font-size:16px\">Operator</span></li>\n	<li><span style=\"font-size:16px\">Peminjam</span></li>\n</ol>\n");



DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` int(11) NOT NULL,
  `status` enum('Y','N') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_inventaris` (`id_inventaris`),
  CONSTRAINT `detail_pinjam_ibfk_1` FOREIGN KEY (`id_inventaris`) REFERENCES `inventaris` (`id_inventaris`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("1","1","22","1","N");
INSERT INTO detail_pinjam VALUES("2","1","23","1","Y");
INSERT INTO detail_pinjam VALUES("3","2","22","1","N");
INSERT INTO detail_pinjam VALUES("4","3","23","4","Y");
INSERT INTO detail_pinjam VALUES("5","3","26","1","Y");
INSERT INTO detail_pinjam VALUES("6","4","27","5","Y");
INSERT INTO detail_pinjam VALUES("7","5","28","2","Y");
INSERT INTO detail_pinjam VALUES("8","6","22","1","N");
INSERT INTO detail_pinjam VALUES("9","6","23","1","N");
INSERT INTO detail_pinjam VALUES("10","6","24","1","N");
INSERT INTO detail_pinjam VALUES("11","7","26","1","Y");
INSERT INTO detail_pinjam VALUES("12","7","27","2","Y");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kondisi` varchar(20) NOT NULL,
  `keterangan_inventaris` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_petugas` (`id_petugas`),
  CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`) ON UPDATE CASCADE,
  CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`) ON UPDATE CASCADE,
  CONSTRAINT `inventaris_ibfk_3` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("22","Laptop","Baik","Barang Masuk","209","1","2018-12-05","1","INV0001","1");
INSERT INTO inventaris VALUES("23","Papan Tulis","Baik","Barang Masuk","290","5","2018-11-07","3","INV0002","1");
INSERT INTO inventaris VALUES("24","Proyektor","Baik","Barang Masuk","239","1","2019-03-07","2","INV0003","1");
INSERT INTO inventaris VALUES("26","Headshet","Baik","Barang Masuk","300","1","2019-03-08","1","INV0004","1");
INSERT INTO inventaris VALUES("27","Meja","Baik","Barang Masuk","310","5","2019-03-30","3","INV0005","1");
INSERT INTO inventaris VALUES("28","Kursi","Baik","Barang Masuk","290","5","2019-04-03","1","INV0006","1");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(35) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(45) NOT NULL,
  `kode_jenis` varchar(45) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("1","Elektronik","K-001","Beli Barang");
INSERT INTO jenis VALUES("5","Kayu","K-004","Beli Barang");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(35) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","administrator");
INSERT INTO level VALUES("2","operator");
INSERT INTO level VALUES("3","peminjam");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("1","Lastri Fauziah","1150701","Ciomas Permai");
INSERT INTO pegawai VALUES("6","Resky Saepudin","90189829","Cibeureum");
INSERT INTO pegawai VALUES("7","Nurul Fitri R","0001232","Pagelaran");
INSERT INTO pegawai VALUES("8","heru","03909209","sbj");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `kode_pinjam` varchar(15) NOT NULL,
  `tanggal_pinjam` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `tanggal_kembali` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status_peminjaman` enum('Pinjam','Kembali') NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_pegawai` (`id_pegawai`),
  CONSTRAINT `peminjaman_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("1","PNJ0001","2019-04-04 06:24:51","0000-00-00 00:00:00","Pinjam","1");
INSERT INTO peminjaman VALUES("2","PNJ0002","2019-04-04 13:58:35","2019-04-04 08:58:35","Kembali","1");
INSERT INTO peminjaman VALUES("3","PNJ0003","2019-04-04 07:02:40","0000-00-00 00:00:00","Pinjam","1");
INSERT INTO peminjaman VALUES("4","PNJ0004","2019-04-04 07:03:09","0000-00-00 00:00:00","Pinjam","1");
INSERT INTO peminjaman VALUES("5","PNJ0005","2019-04-04 07:03:21","0000-00-00 00:00:00","Pinjam","1");
INSERT INTO peminjaman VALUES("6","PNJ0006","2019-04-04 21:18:04","2019-04-04 16:18:04","Kembali","1");
INSERT INTO peminjaman VALUES("7","PNJ0007","2019-04-04 14:18:37","0000-00-00 00:00:00","Pinjam","1");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(35) NOT NULL,
  `password` varchar(35) NOT NULL,
  `nama_petugas` varchar(40) NOT NULL,
  `id_level` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `banned` enum('Y','N') NOT NULL,
  `logintime` tinyint(2) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_pegawai` (`id_level`),
  CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","admin","0192023a7bbd73250516f069df18b500","Admin","1","muhamadwahyudin015@gmail.com","N","0");
INSERT INTO petugas VALUES("6","operator","2407bd807d6ca01d1bcd766c730cec9a","Operator","2","reskysaepudin.23@gmail.com","N","0");
INSERT INTO petugas VALUES("7","peminjam","0192023a7bbd73250516f069df18b500","Peminjam","3","muhammadariff722@gmail.com","N","0");
INSERT INTO petugas VALUES("16","heru","a648ab9a3e32c5f3f6e9ddbd41c0530f","heru","2","asadkjb","N","0");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(20) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(15) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Lab","LB-001","Beli Barang");
INSERT INTO ruang VALUES("2","TU","TU-001","Masukan Barang");
INSERT INTO ruang VALUES("3","Kelas","KLS-001","Beli Barang");
INSERT INTO ruang VALUES("4","Kantor","KTR-003","Beli Barang");
INSERT INTO ruang VALUES("5","Perpustakaan","PRPS-005","Beli Barang");



DROP TABLE smtr_detail;

CREATE TABLE `smtr_detail` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




