<!DOCTYPE html>
<html>
<head>
  <title>Inventaris SMK</title>
</head>
<body>
  <style type="text/css">
    body{
      font-family: sans-serif;
    }
    table{
      margin: 20px auto;
      border-collapse: collapse;
    }
    table th,
    table td{
      border: 1px solid #3c3c3c;
      padding: 3px 8px;

    }
    a{
      background: blue;
      color: #fff;
      padding: 8px 10px;
      text-decoration: none;
      border-radius: 2px;
    }
  </style>

  <?php
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Data Barang.xls");
  ?>

  <center>
    <h1>Data Peminjam </h1>
  </center>

  <table border="1">
   <thead>
    <tr>
     <th>No</th>
     <th>Kode Pinjam</th>
     <th>Nama Barang</th>
     <th>Jumlah Pinjam</th>
     <th>Tanggal Pinjam</th>
     <th>Tanggal Kembali</th>
     <th>Nama Pegawai</th>

   </tr>
 </tr>
</thead>
<tbody>
  <?php
  include 'koneksi.php';
  $no =1;
  $tanggal_awal = $_POST['tgl_a'];
  $tanggal_akhir = $_POST['tgl_b'];
  $data = mysql_query("select * from peminjaman left join detail_pinjam on detail_pinjam.id_detail_pinjam=peminjaman.id_peminjaman 
                    left join inventaris on detail_pinjam.id_inventaris=inventaris.id_inventaris 
                    left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai
                                             
                                            where tanggal_pinjam between '$tanggal_awal' and '$tanggal_akhir' ");
  while($r = mysql_fetch_array($data)){
    ?>

    <tr>
      <td><?php echo $no++;?></td>
      <td><?php echo $r['kode_pinjam']; ?></td>
      <td><?php echo $r['nama']; ?></td>
      <td><?php echo $r['jumlah_pinjam']; ?></td>
      <td><?php echo $r['tanggal_pinjam']; ?></td>
      <td><?php echo $r['tanggal_kembali']; ?></td>
      <td><?php echo $r['nama_pegawai']; ?></td>
    </tr>
    <?php
  }
  ?>
</tbody>
</table>

</body>
</html>