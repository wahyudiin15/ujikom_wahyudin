<?php
$connect = new PDO("mysql:host=localhost;dbname=ujikom_wahyudin", "root", "");
$get_all_table_query = "SHOW TABLES";
$statement = $connect->prepare($get_all_table_query);
$statement->execute();
$result = $statement->fetchAll();

if(isset($_POST['table']))
{
 $output = '';
 foreach($_POST["table"] as $table)
 {
  $show_table_query = "SHOW CREATE TABLE " . $table . "";
  $statement = $connect->prepare($show_table_query);
  $statement->execute();
  $show_table_result = $statement->fetchAll();

  foreach($show_table_result as $show_table_row)
  {
   $output .= "\n\n" . $show_table_row["Create Table"] . ";\n\n";
  }
  $select_query = "SELECT * FROM " . $table . "";
  $statement = $connect->prepare($select_query);
  $statement->execute();
  $total_row = $statement->rowCount();

  for($count=0; $count<$total_row; $count++)
  {
   $single_result = $statement->fetch(PDO::FETCH_ASSOC);
   $table_column_array = array_keys($single_result);
   $table_value_array = array_values($single_result);
   $output .= "\nINSERT INTO $table (";
   $output .= "" . implode(", ", $table_column_array) . ") VALUES (";
   $output .= "'" . implode("','", $table_value_array) . "');\n";
  }
 }
 $file_name = 'inventaris_smk_' . date('y-m-d') . '.sql';
 $file_handle = fopen($file_name, 'w+');
 fwrite($file_handle, $output);
 fclose($file_handle);
 header('Content-Description: File Transfer');
 header('Content-Type: application/octet-stream');
 header('Content-Disposition: attachment; filename=' . basename($file_name));
 header('Content-Transfer-Encoding: binary');
 header('Expires: 0');
 header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file_name));
    ob_clean();
    flush();
    readfile($file_name);
    unlink($file_name);
}

?>
<?php
include ('cek.php');

error_reporting(0);
session_start();

include ('cek_level');
?>

<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Inventaris</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

    <script src="lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $(".knob").knob();
        });
    </script>


    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
  
    

</head>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
    #line-chart {
        height:300px;
        width:800px;
        margin: 0px auto;
        margin-top: 1em;
    }
    .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
        color: #fff;
    }
</style>

<script type="text/javascript">
    $(function() {
        var uls = $('.sidebar-nav > ul > *').clone();
        uls.addClass('visible-xs');
        $('#main-menu').append(uls.clone());
    });
</script>

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <!-- Le fav and touch icons -->
  <link rel="shortcut icon" href="../assets/ico/favicon.ico">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
      <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
          <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
              <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
                  <!--[if (gt IE 9)|!(IE)]><!--> 
                  
                  <!--<![endif]-->

                  <div class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="" href="index.php"><span class="navbar-brand"><span class="fa fa-briefcase"></span> Inventaris Sarana dan Prasarana</span></a></div>

                    <div class="navbar-collapse collapse" style="height: 1px;">

                      <ul id="main-menu" class="nav navbar-nav navbar-right">
                        <li class="dropdown hidden-xs">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i> <?php echo $_SESSION['nama_petugas'];?> 
                                <i class="fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                               
                                <li>
                                <a href="#myModal" role="button" data-toggle="modal">
                                      <i class="ace-icon fa fa-sign-out"></i>
                                      Logout
                                  </a>
                              </li>
                          </ul>
                      </li>
                  </ul>

              </div>
          </div>
      </div>
      
      <div class="sidebar-nav">
       <?php
       
       if ($_SESSION['id_level']==1){
        
        echo'
          <ul>
        <li><a href="index.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i>&nbsp Dashboard</a></li>
        <li>
        <li><a href="#" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-briefcase"></i> Inventarisir<i class="fa fa-collapse"></i></a></li>
        <li><ul class="dashboard-menu nav nav-list collapse">
        <li ><a href="inventaris.php"><span class="fa fa-caret-right"></span> Data Barang</a></li>
        <li><a href="jenis.php"><span class="fa fa-caret-right"></span> Data Jenis</a></li>
        <li ><a href="ruang.php"><span class="fa fa-caret-right"></span> Data Ruang</a></li>
        </ul></li>
        <li><a href="peminjaman.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-plus"></i>&nbsp Peminjaman</a></li>
        <li>
        <li><a href="pengembalian.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class=" fa fa-fw fa-reply"></i>&nbsp Pengembalian</a></li>
        <li>
        <li><a href="#" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-paste"></i> Laporan<i class="fa fa-collapse"></i></a></li>
        <li><ul class="dashboard-menu nav nav-list collapse">
        <li ><a href="inventaris.php"><span class="fa fa-caret-right"></span> Laporan Inventaris</a></li>
        <li><a href="peminjaman.php"><span class="fa fa-caret-right"></span> Laporan Peminjaman</a></li>
        <li ><a href="pengembalian.php"><span class="fa fa-caret-right"></span> Laporan Pengembalian</a></li>
        </ul></li>
        
        <li>
        <li><a href="#" data-target=".accounts-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-list"></i> Lainnya<i class="fa fa-collapse"></i></a></li>
        <li><ul class="accounts-menu nav nav-list collapse">
         <li ><a href="petugas.php"><span class="fa fa-caret-right"></span> Petugas</a></li>
        <li><a href="pegawai.php"><span class="fa fa-caret-right"></span> Pegawai</a></li>
        <li><a href="backup.php"><span class="fa fa-caret-right"></span> Backup</a></li>
        </ul></li>
        </div>
        
        </ul></li>
        
        
        </ul></li>';
    }
    
    elseif ($_SESSION['id_level']==2){
       echo'
       <ul>
       <li><a href="index.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i>&nbsp Dashboard</a></li>
       <li>
       <li><a href="peminjaman.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-plus"></i>&nbsp Peminjaman</a></li>
       <li>
       <li><a href="pengembalian.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-reply"></i>&nbsp Pengembalian</a></li>
       <li></ul>';
   }
   elseif ($_SESSION['id_level']==3){
       echo'<ul>
       <li><a href="index.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i>&nbsp Dashboard</a></li>
       <li>
       <li><a href="peminjaman.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-plus"></i>&nbsp Peminjaman</a></li>
       <li></ul>';
   }
   ?>
   
   
   
   
</div>

<div class="content">
    <div class="header">


        <h1 class="page-title">Backup</h1>
        <ul class="breadcrumb">

        </ul>

    </div>
    <div class="main-content">

    </div>  <div class="panel-body">
       <div class="row">
        <div class="col-lg-12">

                <div class="row-fluid">	
				<div class="box span12">
					<div class="box-header">
						<h2>Backup To Database</h2>
					</div>
					<div class="box-content">
						<div class="alert alert-block span5 pull-right"> 
							
							<h3 class="alert-heading">Warning!</h3>
							<p align="black">Jika Ingin MemBackup Database Per Tabel atau Semua silahkan klik tombol Export dibawah!</p>
						</div>
    <h3 align="center">Backup Database</h3>
    <br />
    <form method="post" id="export_form" >
     <h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pilih Tabel Untuk Export</h4>
    <?php
    foreach($result as $table)
    {
    ?>
     <div class="checkbox">
      <label><input type="checkbox" class="checkbox_table" name="table[]" value="<?php echo $table["Tables_in_ujikom_wahyudin"]; ?>" /> <?php echo $table["Tables_in_ujikom_wahyudin"]; ?></label>
     </div>
    <?php
    }
    ?>
     <div class="form-group">
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" id="submit" class="btn btn-danger" value="Export" />
     </div>
      <div class="box-content">
            <div class=""> 
              
            </div>
    <h2>&nbsp;&nbsp;Backup Database Semua</h2>
     <div class="form-group">
      <?php
                              error_reporting(0);
                              $file=date("Ymd").'_backup_database_'.time().'.sql';
                              backup_tables("localhost","root","","ujikom_wahyudin",$file);
                            ?>
                                <div class="form-group pull-left">
                                  &nbsp;&nbsp;&nbsp;&nbsp;<a style="cursor:pointer" onclick="location.href='download_backup_data.php?nama_file=<?php echo $file;?>'" title="Download" class="btn btn-primary" > Export</a>
                                </div> 
                                <?php
            /*
            untuk memanggil nama fungsi :: jika anda ingin membackup semua tabel yang ada didalam database, biarkan tanda BINTANG (*) pada variabel $tables = '*'
            jika hanya tabel-table tertentu, masukan nama table dipisahkan dengan tanda KOMA (,) 
            */
            function backup_tables($host,$user,$pass,$name,$nama_file,$tables ='*') {
              $link = mysql_connect($host,$user,$pass);
              mysql_select_db($name,$link);
              
              if($tables == '*'){
                $tables = array();
                $result = mysql_query('SHOW TABLES');
                while($row = mysql_fetch_row($result)){
                  $tables[] = $row[0];
                }
              }
              else{//jika hanya table-table tertentu
                $tables = is_array($tables) ? $tables : explode(',',$tables);
              }
              
              foreach($tables as $table){
                $result = mysql_query('SELECT * FROM '.$table);
                $num_fields = mysql_num_fields($result);
                                
                $return.= 'DROP TABLE '.$table.';';//menyisipkan query drop table untuk nanti hapus table yang lama
                $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
                $return.= "\n\n".$row2[1].";\n\n";
                
                for ($i = 0; $i < $num_fields; $i++) {
                  while($row = mysql_fetch_row($result)){
                    //menyisipkan query Insert. untuk nanti memasukan data yang lama ketable yang baru dibuat
                    $return.= 'INSERT INTO '.$table.' VALUES(';
                    for($j=0; $j<$num_fields; $j++) {
                      //akan menelusuri setiap baris query didalam
                      $row[$j] = addslashes($row[$j]);
                      $row[$j] = ereg_replace("\n","\\n",$row[$j]);
                      if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                      if ($j<($num_fields-1)) { $return.= ','; }
                    }
                    $return.= ");\n";
                  }
                }
                $return.="\n\n\n";
              }             
              //simpan file di folder
              $nama_file;
              
              $handle = fopen('backup/'.$nama_file,'w+');
              fwrite($handle,$return);
              fclose($handle);
            }
            ?>
    </form>
   </div>
  </div>
 </body>
</html>
<script>
$(document).ready(function(){
 $('#submit').click(function(){
  var count = 0;
  $('.checkbox_table').each(function(){
   if($(this).is(':checked'))
   {
    count = count + 1;
   }
  });
  if(count > 0)
  {
   $('#export_form').submit();
  }
  else
  {
   alert("Please Select Atleast one table for Export");
   return false;
  }
 });
});
</script>

</div>
</div>
</div>
</div>
</div>
</div>

<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
 <div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Logout Confirmation</h3>
        </div>
        <div class="modal-body">
            <p class="error-text"><i class="fa fa-warning modal-icon"></i><?php echo $_SESSION['nama_petugas'];?> yakin ingin keluar dari webiste ini?
        </div>
        Silahkan Klik Button Logout
        <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Kembali</button>
            <a class="btn btn-warning" href="logout.php">Logout</a>
           
        </div>
      </div>
    </div>
</div>


<footer>
    <hr>

    <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
    <p class="pull-right">Inventaris</a> 
        <p> © 2019 Sarana & Prasarana</a></p>
    </footer>
</div>
</div>


<script src="lib/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
    $("[rel=tooltip]").tooltip();
    $(function() {
        $('.demo-cancel-click').click(function(){return false;});
    });
</script>

</body>

</html>